#include <iostream>
#include "myarray.h"

int main() {
    array<int, 50> arr{};

    int add = 0;
    for (auto i = arr.begin(); i != arr.end(); i++ ) {
        *i = add;
        add++;
    }

    for (auto i : arr) {
        printf("%d\n", i);
    }


    return 0;
}

