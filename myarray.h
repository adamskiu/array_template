#ifndef ARRAY_INCLUDED
#define ARRAY INCLUDED

#include <iostream>
#include <string>
#include <stdexcept>
#include <sstream>

template<class T>
std::string to_string(const T& t)
{
    std::ostringstream os;
    os << t;
    return os.str();
}

struct Range_error : std::out_of_range {
    int index;
    Range_error(int i) :out_of_range("Range error: " + to_string(i)), index(i) { }
};


template <class T, int N>
struct array {
    typedef T  value_type;
    typedef T* iterator;
    typedef const T* const_iterator;
    typedef unsigned int size_type;

    T elems[N];

    // explicit constructor, copy and delete operatrion are not needed

    iterator       begin()       { return elems; }
    const_iterator begin() const { return elems; }
    iterator       end()       { return elems + N; }
    const_iterator end() const { return elems + N; }

    size_type size() const { return N; }

    T&       operator[] (int n)       { return elems[n]; }
    const T& operator[] (int n) const { return elems[n]; }

    T& at (int n)
    {
        if (n < 0 || N <= n) throw Range_error(n);
        return elems[n];
    }

    const T& at (int n) const
    {
        if (n < 0 || N <= n) throw Range_error(n);
        return elems[n];
    }

    T*       data()       { return elems; }
    const T* data() const { return elems; }

};

#endif

